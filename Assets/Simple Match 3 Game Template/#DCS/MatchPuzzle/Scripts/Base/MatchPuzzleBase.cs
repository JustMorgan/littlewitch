﻿using UnityEngine;

using System;
using System.Collections.Generic;

namespace DCS.MatchPuzzle.Base
{
	#region Custom

	#region Block Type

	public enum BlockType : int
	{
		Red,
		Green,
		Blue,
		Yellow,
	}

	#endregion
	#region Constants

	static class Constants
	{
		public const int minimumMatch = 3;// minimum match count
		
		public const float destroyNormalDuration = .3f;// block destroy duration
		public const float destroyUltimateItemTick = .1f;// to destroy one by one when using Ultimate Item
		public const float changeToItemBlockDuration = .2f;// after the destruction of Item to change duration
		public const float moveDuration = .3f;// block move duration
		public const float swapDuration = .2f;// block swap duration
		public const float blocksOnDuration = 1f;// blocks on duration

		public const int destroyScoreNormal = 100;// block destroy score default values
		public const int destroyScoreVerticalAndHorizontal = 110;
		public const int destroyScoreBox = 120;
		public const int destroyScoreUltimate = 500;

		public const float comboCheckMaxTime = 1.3f;// combo time..
		public const float comboFactor = 0.1f;// match point need combo factor
	}

	#endregion

	#endregion

	#region Algorythm Part

	#region Enums

	public enum ItemCombineType : int
	{
		None,
		Both,// horizontal + vertical or horizontal + horizontal or vertical + vertical
		BoxAndBoth,// horizontal + box or vertical + box
		BoxAndBox,// box + box
		Ultimate,// ultimate + none
		UltimateAndBoth,// ultimate + horizontal or ultimate + vertical
		UltimateAndBox,// ultimate + box
		UltimateAndUltimate,// ultimate + ultimate
	}

	public enum ItemType : int
	{
		None = 0,
		Horizontal = 1,	// horizontal line destroy
		Vertical = 2,	// vertical line destroy
		Box = 3,		// box shape destroy
		Ultimate = 4,	// same type destroy
	}

	public enum MatchType : int
	{
		None,
		Match3Horizontal,
		Match3Vertical,
		Match4Horizontal,
		Match4Vertical,
		MatchBoth,
		Match5,
	}

	public enum DestroyType : int
	{
		Normal,
		Horizontal, Vertical,
		Box,
		Ultimate,
	}

	#endregion

	#region Structs

	public struct Index
	{
		public int row;
		public int column;

		public Index(int row, int column)
		{
			this.row = row;
			this.column = column;
		}
	}

	#endregion

	#endregion
}
