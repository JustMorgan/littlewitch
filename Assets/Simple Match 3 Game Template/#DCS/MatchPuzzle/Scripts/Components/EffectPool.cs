﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;// for List<>

public class EffectPool : MonoBehaviour
{
	/// How To Use
	/// 
	/// 'UI Canvas' in 'Match Container' of the 'Effect Container' in the 'Effect' will 'Pool' format management to be 'empty gameObject' to create, under the 'Effect Samples region' of the form used to refer to extend it.
	/// 
	/// [GameObject] prefab... (Fields) : Connect the 'Prefab' effects.
	/// [int] ...Size (Fields) : Directly specify the size of the Pool.
	/// [List<GameObject>] (Fields) : In order to manage a list of effect object created.
	/// [Transform] ...Container (Fields) : Parents 'transform' the effect of the resulting object.
	/// 
	/// ...Init (Functions) : Initialization of the effect.
	/// Use... (Functions) : When use directly from another component.

	/// 사용 방법
	/// (Pool 직접 만들기 힘드신분은 참고하세여)
	/// 
	/// UI Canvas 의 MatchContainer 의 EffectContainer 안에
	/// Effect 들이 Pool 형식으로 관리될 empty gameObject 를 만들고
	/// 아래 Effect Samples region 의 형식을 참고하여 확장해 사용
	/// 
	/// [GameObject] prefab... 필드들 : 이펙트의 프리팹을 연결
	/// [int] ...Size 필드들 : Pool 의 크기를 직접 지정
	/// [List<GameObject>] 필드들 : 생성 된 이펙트 오브젝트들의 리스트를 관리하기 위함
	/// [Transform] ...Container 필드들 : 생성 된 이펙트 오브젝트들의 부모 transform
	/// 
	/// ...Init 함수들 : 이펙트 초기화용
	/// Use... 함수들 : 다른 컴포넌트에서 직접 불러올 때 사용

	#region Effect Samples

	// 가로 1 줄 사라지는 이펙트
	public GameObject prefabItemHorizontal;
	public int itemHorizontalSize;
	List<GameObject> itemHorizontal = new List<GameObject>();
	public Transform itemHorizontalContainer;
	void ItemHorizontalInit()
	{
		if (itemHorizontal.Count != 0) return;
		for (int i = 0; i < itemHorizontalSize; i++)
		{
			GameObject obj = Instantiate(prefabItemHorizontal) as GameObject;
			obj.transform.parent = itemHorizontalContainer;
			obj.SetActive(false);
			itemHorizontal.Add(obj);
		}
	}
	public void UseItemHorizontal(Vector3 position)
	{
		for (int i = 0; i < itemHorizontal.Count; i++)
		{
			if (!itemHorizontal[i].activeInHierarchy)
			{
				itemHorizontal[i].transform.localPosition = position;
				itemHorizontal[i].SetActive(true);
				break;
			}
		}
	}

	// 가로 3 줄 사라지는 이펙트
	public GameObject prefabItemHorizontal3;
	public int itemHorizontal3Size;
	List<GameObject> itemHorizontal3 = new List<GameObject>();
	public Transform itemHorizontal3Container;
	void ItemHorizontal3Init()
	{
		if (itemHorizontal3.Count != 0) return;
		for (int i = 0; i < itemHorizontal3Size; i++)
		{
			GameObject obj = Instantiate(prefabItemHorizontal3) as GameObject;
			obj.transform.parent = itemHorizontal3Container;
			obj.SetActive(false);
			itemHorizontal3.Add(obj);
		}
	}
	public void UseItemHorizontal3(Vector3 position)
	{
		for (int i = 0; i < itemHorizontal3.Count; i++)
		{
			if (!itemHorizontal3[i].activeInHierarchy)
			{
				itemHorizontal3[i].transform.localPosition = position;
				itemHorizontal3[i].SetActive(true);
				break;
			}
		}
	}

	// 세로 1 줄 사라지는 이펙트
	public GameObject prefabItemVertical;
	public int itemVerticalSize;
	List<GameObject> itemVertical = new List<GameObject>();
	public Transform itemVerticalContainer;
	void ItemVerticalInit()
	{
		if (itemVertical.Count != 0) return;
		for (int i = 0; i < itemVerticalSize; i++)
		{
			GameObject obj = Instantiate(prefabItemVertical) as GameObject;
			obj.transform.parent = itemVerticalContainer;
			obj.SetActive(false);
			itemVertical.Add(obj);
		}
	}
	public void UseItemVertical(Vector3 position)
	{
		for (int i = 0; i < itemVertical.Count; i++)
		{
			if (!itemVertical[i].activeInHierarchy)
			{
				itemVertical[i].transform.localPosition = position;
				itemVertical[i].SetActive(true);
				break;
			}
		}
	}

	// 세로 3 줄 사라지는 이펙트
	public GameObject prefabItemVertical3;
	public int itemVertical3Size;
	List<GameObject> itemVertical3 = new List<GameObject>();
	public Transform itemVertical3Container;
	void ItemVertical3Init()
	{
		if (itemVertical3.Count != 0) return;
		for (int i = 0; i < itemVertical3Size; i++)
		{
			GameObject obj = Instantiate(prefabItemVertical3) as GameObject;
			obj.transform.parent = itemVertical3Container;
			obj.SetActive(false);
			itemVertical3.Add(obj);
		}
	}
	public void UseItemVertical3(Vector3 position)
	{
		for (int i = 0; i < itemVertical3.Count; i++)
		{
			if (!itemVertical3[i].activeInHierarchy)
			{
				itemVertical3[i].transform.localPosition = position;
				itemVertical3[i].SetActive(true);
				break;
			}
		}
	}

	// 박스 모양 사라지는 이펙트
	public GameObject prefabItemBox;
	public int itemBoxSize;
	List<GameObject> itemBox = new List<GameObject>();
	public Transform itemBoxContainer;
	void ItemBoxInit()
	{
		if (itemBox.Count != 0) return;
		for (int i = 0; i < itemBoxSize; i++)
		{
			GameObject obj = Instantiate(prefabItemBox) as GameObject;
			obj.transform.parent = itemBoxContainer;
			obj.SetActive(false);
			itemBox.Add(obj);
		}
	}
	public void UseItemBox(Vector3 position)
	{
		for (int i = 0; i < itemBox.Count; i++)
		{
			if (!itemBox[i].activeInHierarchy)
			{
				itemBox[i].transform.localPosition = position;
				itemBox[i].SetActive(true);
				break;
			}
		}
	}

	// 큰 박스 모양 사라지는 이펙트
	public GameObject prefabItemBoxAndBox;
	public int itemBoxAndBoxSize;
	List<GameObject> itemBoxAndBox = new List<GameObject>();
	public Transform itemBoxAndBoxContainer;
	void ItemBoxAndBoxInit()
	{
		if (itemBoxAndBox.Count != 0) return;
		for (int i = 0; i < itemBoxAndBoxSize; i++)
		{
			GameObject obj = Instantiate(prefabItemBoxAndBox) as GameObject;
			obj.transform.parent = itemBoxAndBoxContainer;
			obj.SetActive(false);
			itemBoxAndBox.Add(obj);
		}
	}
	public void UseItemBoxAndBox(Vector3 position)
	{
		for (int i = 0; i < itemBoxAndBox.Count; i++)
		{
			if (!itemBoxAndBox[i].activeInHierarchy)
			{
				itemBoxAndBox[i].transform.localPosition = position;
				itemBoxAndBox[i].SetActive(true);
				break;
			}
		}
	}

	// 같은 BlockType 사라지는 이펙트 (지속)
	public GameObject prefabItemUltimate;
	public int itemUltimateSize;
	List<GameObject> itemUltimate = new List<GameObject>();
	public Transform itemUltimateContainer;
	void ItemUltimateInit()
	{
		if (itemUltimate.Count != 0) return;
		for (int i = 0; i < itemUltimateSize; i++)
		{
			GameObject obj = Instantiate(prefabItemUltimate) as GameObject;
			obj.transform.parent = itemUltimateContainer;
			obj.SetActive(false);
			itemUltimate.Add(obj);
		}
	}
	public void UseItemUltimate(Vector3 position, float activeTime)
	{
		StartCoroutine(ItemUltimate_C(position, activeTime));
	}
	IEnumerator ItemUltimate_C(Vector3 position, float activeTime)
	{
		for (int i = 0; i < itemUltimate.Count; i++)
		{
			if (!itemUltimate[i].activeInHierarchy)
			{
				itemUltimate[i].transform.localPosition = position;
				itemUltimate[i].SetActive(true);
				yield return new WaitForSeconds(activeTime);
				itemUltimate[i].SetActive(false);
				yield break;
			}
		}
	}

	// 같은 BlockType 사라지는 이펙트 (순간)
	public GameObject prefabItemUltimateLine;
	public int itemUltimateLineSize;
	List<LineRenderer> itemUltimateLine = new List<LineRenderer>();
	public Transform itemUltimateLineContainer;
	void ItemUltimateLineInit()
	{
		if (itemUltimateLine.Count != 0) return;
		for (int i = 0; i < itemUltimateLineSize; i++)
		{
			GameObject obj = Instantiate(prefabItemUltimateLine) as GameObject;
			obj.transform.parent = itemUltimateLineContainer;
			obj.transform.localScale = Vector3.one;
			obj.transform.localPosition = Vector3.zero;
			itemUltimateLine.Add(obj.GetComponent<LineRenderer>());
			obj.SetActive(false);
		}
	}
	public void UseItemUltimateLine(Vector3 from, Vector3 to)
	{
		for (int i = 0; i < itemUltimateLine.Count; i++)
		{
			if (!itemUltimateLine[i].gameObject.activeInHierarchy)
			{
				itemUltimateLine[i].SetPosition(0, from);
				itemUltimateLine[i].SetPosition(1, to);
				itemUltimateLine[i].gameObject.SetActive(true);
				break;
			}
		}
	}

	#endregion

	#region Init

	void Start()
	{
		ItemHorizontalInit();
		ItemHorizontal3Init();
		ItemVerticalInit();
		ItemVertical3Init();
		ItemBoxInit();
		ItemBoxAndBoxInit();
		ItemUltimateInit();
		ItemUltimateLineInit();
	}

	#endregion
}
